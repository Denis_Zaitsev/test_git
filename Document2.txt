User@WIN-49PU9U8QOUK MINGW64 /d/Progects
$ git clone https://DenisZaitsev@bitbucket.org/DenisZaitsev/test_git.git
Cloning into 'test_git'...
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git add NewTextDocument.txt

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
 (use "git reset HEAD <file>..." to unstage)

 new file: NewTextDocument.txt

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git commit -m "add file NewTextDocument.txt"
[master 6917a62] add file NewTextDocument.txt
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 NewTextDocument.txt

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git push

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git commit -a
[master ed59b09] Add 1 string into the txt file
 1 file changed, 1 insertion(+)

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git push
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 323 bytes | 323.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/DenisZaitsev/test_git.git
 6917a62..ed59b09 master -> master

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git commit -a
[master 116c537] Add a second string into the txt file
 1 file changed, 2 insertions(+), 1 deletion(-)

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git push
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 338 bytes | 338.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/DenisZaitsev/test_git.git
 ed59b09..116c537 master -> master

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git branch develop

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git checkout develop
Switched to branch 'develop'

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (develop)
$ git commit -a
[develop 91e80a9] Add 3-rd text line into txt file
 1 file changed, 3 insertions(+), 2 deletions(-)

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (develop)
$ git push --set-upstream origin develop
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 336 bytes | 336.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
remote:
remote: Create pull request for develop:
remote: https://bitbucket.org/DenisZaitsev/test_git/pull-requests/new?source=develop&t=1
remote:
To https://bitbucket.org/DenisZaitsev/test_git.git
 * [new branch] develop -> develop
Branch 'develop' set up to track remote branch 'develop' from 'origin'.

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (develop)
$ git commit -a
[develop da0b17b] Delete 3-rd text line from txt file
 1 file changed, 1 insertion(+), 2 deletions(-)

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (develop)
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git merge develop
Updating 116c537..da0b17b
Fast-forward
 NewTextDocument.txt | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git push
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 331 bytes | 165.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/DenisZaitsev/test_git.git
 116c537..da0b17b master -> master

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (master)
$ git checkout develop
Switched to branch 'develop'
Your branch is ahead of 'origin/develop' by 1 commit.
  (use "git push" to publish your local commits)

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (develop)
$ git push
Total 0 (delta 0), reused 0 (delta 0)
remote:
remote: Create pull request for develop:
remote:   https://bitbucket.org/DenisZaitsev/test_git/pull-requests/new?source=develop&t=1
remote:
To https://bitbucket.org/DenisZaitsev/test_git.git
   91e80a9..da0b17b  develop -> develop

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (develop)
$ git add Document2.txt

User@WIN-49PU9U8QOUK MINGW64 /d/Progects/test_git (develop)
$ git commit -a
[develop 0903665] Create file Document2.txt
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 Document2.txt